
Which of the following phrases is false?

a) A concurrent algorithm is a formal description of the behavior of a set of sequential state machines that cooperate through a communication medium.
b) An algorithm may also be called a program.
c) In a multiprocess program, each process corresponding to the concurrent execution of a given state machine.
d) A process is an instance of a sequential algorithm.

R: c (not only one state machine, each process has its own).

末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末�

Which of the following invariants referring a queue is FALSE?
#p = number of data items produced so far 
#c = number of data items consumed so far 
#n = number of elements currently in the queue
k = size of the queue

a) #n = #p - #c
b) (#n > 0) => (#p > #c)
c) k = #n
d) (#c >= 0) && (#p >= #c) && (#p <= k)

R: d (#p pode ser maior que a queue).

末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末�

Which of the following IS NOT part of the sequential specification of a lock object?

a) For any invocation of lock.release() there must be a corresponding previous invocation of lock.acquire().
b) From an external observer point of view, all the lock.acquire() and lock.release() invocations appear as if they have been invoked one after the other.
c) On returning from lock.acquire() the object state is always locked.
d) On returning from lock.release() the object state is always free.

R: a?

末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末�

Given the following implementation of a mutex (i is the process acquiring/releasing the mutex, j is the other process), indicate which of the phrases below is TRUE.

operation mutex_acquire(i) is
    FLAG[i] = up;
    while (FLAG[j] == up) do
        FLAG[i] = down;
        sleep (small_random_time);
        FLAG[i] = up;
    done
end
operation mutex_release(i) is
    FLAG[i] = down;
end

a) The given implementation ensures both mutual exclusion and progress.
b) The given implementation ensures mutual exclusion but not progress.
c) The given implementation ensures progress but not mutual exclusion.
d) The given implementation does not ensure mutual exclusion neither progress.

R: b (may cause deadlock, thus does not ensure progress).

末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末�

Consider the implementation of the TASLock class as presented below. Which of the endings makes the following statement FALSE? "The lock object as defined in the TASLock class..."

public class TASLock(k) implemnets Lock {
    AtomicBoolean state = new AtomicBoolean(false);
    public void Lock() {
        while (!state.getAndSet(true)) {
            // do nothing
        }
    }
    public void unlock() {
        state.set(false);
    }
}

a) Ensures mutual exclusion.
b) Use non-blocking programming techniques.
c) Is a valid implementation for the class Lock.
d) May cause a livelock.

R: c

末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末�

Which of the following lists present the progress properties from the weakest to the strongest?

a) Fairness, Deadlock freedom, Starvation freedom
b) Deadlock freedom, Starvation freedom, Fairness
c) Deadlock freedom, Fairness, Starvation freedom
d) Starvation freedom, Deadlock freedom, Fairness

R: b

末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末�

Finer grain synchronization aims at?

a) Improving liveness.
b) Improving reliability.
c) Improving performance.
d) Improving safety.

R: c

末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末末�

The AtomicMarkableReference<T> class implements the method public boolean compareAndSet(...). Which of the following statements is TRUE?

a) The compareAndSet() method has three arguments, the expected reference, the expected mark, and the new reference.
b) The compareAndSet() method has four arguments, the expected reference, the expected mark, the new reference and the new mark.
c) The compareAndSet() method has two arguments, the expected reference and the new reference.
d) The compareAndSet() method has five arguments, the expected reference, the expected mark, the expected value, the new reference and the new mark.

R: b
