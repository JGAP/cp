#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>
#include <pthread.h>    /* required for pthreads */

#define DIM_MATRIZ (12*1024)
#define MAX_VAL 256


int orig[DIM_MATRIZ*DIM_MATRIZ];      // int orig[DIM_MATRIZ][DIM_MATRIZ]
int destA[DIM_MATRIZ*DIM_MATRIZ];
int destB[DIM_MATRIZ*DIM_MATRIZ];
#define M(mname,mline,mcolumn)   mname[mline*DIM_MATRIZ+mcolumn]


#define MIN(A,B)  (((A) < (B)) ? A : B)
#define IF_FLAG(flag,instruction)  if (flag) {instruction;}


int checkFlag, verboseFlag, timeFlag, nthreadsFlag;


struct BlockInfo {
  int initLine;
  int numLines;
};



long timeElapsed (struct timeval start, struct timeval end) {
  long secs_used,micros_used;

  secs_used = (end.tv_sec - start.tv_sec); //avoid overflow by subtracting first
  micros_used = ((secs_used*1000000) + end.tv_usec) - (start.tv_usec);
  return micros_used;
}

void usage (char *progName) {
  printf ("Usage: %s [-c] [-v] [-t] <N_THREADS>\n", progName);
  exit (1);
}


void  initMatrixes (void) {
  int i, j;
  struct timeval start, end;
  long elapsed;

  IF_FLAG (verboseFlag, gettimeofday(&start, NULL));
    
  // This commented line below is "equivalent" to the folloing loop
  // arc4random_buf (orig, sizeof(orig));
  for (i=0; i<DIM_MATRIZ; i++)
    for (j=0; j<DIM_MATRIZ; j++) {
      M(orig,i,j) = rand() % MAX_VAL;    // orig[i][j] = rand() % MAX_VAL;
      // M(destA,i,j) = M(destB,i,j) = 0;    // destA[i][j] = destB[i][j] = 0;
    }

  memset (destA, 0, sizeof(destA));
  memset (destB, 0, sizeof(destB));
  
  IF_FLAG (verboseFlag, gettimeofday(&end, NULL); elapsed = timeElapsed (start, end));
  IF_FLAG (verboseFlag, printf ("Execution time: InitMatrixes = %6.3lf seconds!\n", elapsed/1000000.0 ));
}

void  transposeBlock (int *src, int *dest, int line, int column, int dim_line, int dim_column) {
  int i, j;
  int minI = MIN(line+dim_line,DIM_MATRIZ), J = MIN(column+dim_column,DIM_MATRIZ);
  for (i=line; i<minI; i++)
    for (j=column; j<J; j++) {
      M(dest,j,i) = M(src,i,j);   // dest[j][i] = src[i][j];
    }
}

void  sequentialVersion (void) {
  transposeBlock (orig, destA, 0, 0, DIM_MATRIZ, DIM_MATRIZ);
}



//*******************************************************************
//DONE IN CLASS
//*******************************************************************
void * funcAux (void * _arg) {
  struct BlockInfo * arg = (struct BlockInfo *) _arg;
  int startLine = arg->initLine;
  int nLines = arg->numLines;

  transposeBlock (orig, destB, startLine, 0, nLines, DIM_MATRIZ);

  return NULL;
}

//*******************************************************************
//DONE IN CLASS
//*******************************************************************
void parallelVersion (int blockSize, int maxthreads) {
  //printf ("ERROR: Please implement yout Parallel version!\n");

  int i;
  pthread_t Threads[maxthreads];
  struct BlockInfo Args[maxthreads];
  int currLine = 0;

  for(i=0; i<maxthreads; i++) {
    Args[i].initLine = currLine;
    Args[i].numLines = blockSize;

    pthread_create( &Threads[i], NULL, funcAux, (void *)&Args[i] );
    currLine += blockSize;
  }

  //fazer o join aqui ou no for anterior logo a seguir ao create?
  for(i=0; i<maxthreads; i++) {
    pthread_join( Threads[i], NULL );
  }
}



int checkVersoesAB (void) {
  int i, j;
  int areEqual = 1;
  for (i=0; i<DIM_MATRIZ && areEqual; i++)
    for (j=0; j<DIM_MATRIZ && areEqual; j++)
      if (M(destA,i,j) != M(destB,i,j))   // destA[i][j] != destB[i][j]
        areEqual = 0;
  return areEqual;
}


// —————————————————————————————————————————————

void doInitMatrixes (void) {
  IF_FLAG (verboseFlag, printf("Init...\n")); 
  initMatrixes ();  
}

int doSequenttialVersion (void) {
  struct timeval tAstart, tAend;
  long timeA = 0;
  
  if (checkFlag) {  
    IF_FLAG (timeFlag || verboseFlag, gettimeofday(&tAstart, NULL));
    sequentialVersion ();
    IF_FLAG (timeFlag || verboseFlag, gettimeofday(&tAend, NULL); timeA = timeElapsed (tAstart, tAend));
    IF_FLAG (verboseFlag, printf ("Execution time: sequentialVersion = %6.3lf seconds!\n", timeA/1000000.0));
  }
  return timeA;
}

int doParallelVersion (int nthreads) {
  struct timeval tBstart, tBend;
  long timeB = 0;
  int blockSize;
  
  blockSize = (DIM_MATRIZ + nthreads - 1) / nthreads;
  assert ((blockSize > 0) && (blockSize <= DIM_MATRIZ));
  
  IF_FLAG (timeFlag || verboseFlag, gettimeofday(&tBstart, NULL));
  parallelVersion (blockSize, nthreads);
  IF_FLAG (timeFlag || verboseFlag, gettimeofday(&tBend, NULL); timeB = timeElapsed (tBstart, tBend));
  IF_FLAG (verboseFlag, printf ("Execution time: parallelVersion = %6.3lf seconds!\n", timeB/1000000.0 ));
  IF_FLAG (timeFlag, printf ("%d\t%d\t%ld\n", nthreads, blockSize, timeB));
  return timeB;
}

void doCheck (void) {
  if (checkFlag) {
    IF_FLAG (verboseFlag, printf("Comparing...\n"));
    if( checkVersoesAB () == 0)
      printf ("ERROR!!!! ERROR!!!! ERROR!!!! ==>>> Matriz A != Matriz B\n");
    else if (verboseFlag) {
      printf ("Matriz A = Matriz B\n");      
    }
  }
}



int main(int argc, char *argv[]) {
  
  long timeA, timeB;
  int nthreads;
  char ch;
  char *progName = argv[0];
  
  checkFlag = verboseFlag = timeFlag = 0;
  nthreadsFlag = 1;

  // process command line options
  while ((ch = getopt(argc, argv, "cvt")) != -1) {
    switch (ch) {
      case 'c':   checkFlag = 1; break;
      case 'v':   verboseFlag = 1; break;
      case 't':   timeFlag = 1; break;
      case '?':
      default:    usage(progName);
    }
  }
  argc -= optind;
  argv += optind;  
  
  // get the number of threads from the command line (mandatory argument)
  if (argc != 1) {
    usage(progName);
  }
  
  nthreads = atoi (argv[0]);
  assert (nthreads > 0  &&  nthreads <= 1024);

  // Do the stuff
  doInitMatrixes ();  
  timeA = doSequenttialVersion ();
  timeB = doParallelVersion (nthreads);
  IF_FLAG (checkFlag && verboseFlag, printf("Speedup(%d) = %5.2f\n", nthreads, (float)timeA/timeB));
  doCheck ();
      
  return 0;  
}
