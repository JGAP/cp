package cp.articlerep.ds;

public class SortedList<T extends Comparable<T>> implements Set<T> {
    
	class Node<T> {
        private final T value;
        private Node<T> next;
        
        public Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }
        
        public T getValue() { return value; }
        public Node<T> getNext() { return next; }
        public void setNext(Node<T> next) { this.next = next; }
    }
	
    private Node<T> head;
    
    public SortedList() {
        head = null;
    }
    
    //verificar ordenacao (sorted)
	//verificar duplicados (set)
    public boolean validate() {
    	Node<T> currentNode = head;
    	Node<T> nextNode = null;
    	
    	if(head!=null) {
    		nextNode = head.getNext();
    		
    		while (nextNode != null) {
    			//duplicate value
    			if(currentNode.getValue().compareTo(nextNode.getValue()) == 0)
    				return false;
    			
    			//not well sorted
    			if(currentNode.getValue().compareTo(nextNode.getValue()) > 0)
    				return false;
    			
                currentNode = nextNode;
                nextNode = nextNode.getNext();
            }
    	}
    	
    	return true;
    }
    
    public boolean insert(T valueToInsert) {
    	if (head == null) {
            head = new Node<T>(valueToInsert, null);
            return true;
        } else if (head.getValue().compareTo(valueToInsert) == 0) {
            return false;
        } else if (head.getValue().compareTo(valueToInsert) > 0) {
            Node<T> nodeToInsert = new Node<T>(valueToInsert, head);
            head = nodeToInsert;
            return true;
        } else {
            Node<T> previousNode = null;
            Node<T> currentNode = head;
            
            while (currentNode != null && currentNode.getValue().compareTo(valueToInsert) < 0) {
                previousNode = currentNode;
                currentNode = currentNode.getNext();
            }
            
            if (currentNode == null || currentNode.getValue().compareTo(valueToInsert) > 0) {
                // value is not in list, insert between previousNode and currentNode
                Node<T> nodeToInsert = new Node<T>(valueToInsert, currentNode);
                previousNode.setNext(nodeToInsert);
                return true;
            } else {
                // currentNode has value
                return false;
            }
        }
    }
    
    public boolean remove(T valueToRemove) {   
    	if (head == null) {
            return false;
        } else if (head.getValue().compareTo(valueToRemove) == 0) {
            head = head.getNext();
            return true;
        } else {
            Node<T> previousNode = head;
            Node<T> currentNode = head.getNext();
            
            while (currentNode != null && currentNode.getValue().compareTo(valueToRemove) < 0) {
                previousNode = currentNode;
                currentNode = currentNode.getNext();
            }
            
            if (currentNode == null || currentNode.getValue().compareTo(valueToRemove) > 0) {
                // value is not in list
                return false;
            } else {
                // currentNode has value, remove it
                previousNode.setNext(currentNode.getNext());
                return true;
            }
        }
    }
    
    public boolean contains(T value) {	
        Node<T> currentNode = head;
        
        while (currentNode != null && currentNode.getValue().compareTo(value) < 0) {
            currentNode = currentNode.getNext();
        }
        
        return currentNode != null && currentNode.getValue().compareTo(value) == 0;
    }
}
