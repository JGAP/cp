#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

bool isInside(double xPos, double yPos){

	double distance = sqrt((xPos * xPos) + (yPos * yPos));
	return (distance < 1.0);

} 

int main(){

	printf("Number of simulations\n");
	int numOfSim;
	scanf("%d", &numOfSim);

	printf("Number of points\n");
	int numOfPoints;
	scanf("%d", &numOfPoints);

	srand( time (NULL));
	double piValue = 0;
	double calcAux = 0;
	int hits = 0;
	
	for(int i = 0; i < numOfSim; i++){
		double xPos = (double)rand()/RAND_MAX * 2.0 - 1.0;
		double yPos = (double)rand()/RAND_MAX * 2.0 - 1.0;
		if(isInside(xPos, yPos)){
			hits++;
		}			
	
	}	
	
	calcAux = ((double)hits / numOfPoints);
	piValue = calcAux * 4;

	printf("Points within circle: %d \n", hits);
	printf("Pi Estimation: %lf \n", piValue);
	printf("Elapsed Time: cenas\n");

}

