#include<stdio.h>
#include<stdlib.h>

int main(){
	
	int n;
	printf("Enter size of array\n");
	scanf("%d", &n);
	int *A = (int*)calloc(n, sizeof(int));

	//if we dont initialize the array with values, with malloc it will print garbage;
	//With calloc we dont nered to initialize the array, calloc ensures that all positions will be initialized with zero;
	for(int i = 0; i < n; i++){
		A[i] = i + 1;	
	}

	free(A);
	A = NULL; //it is a good practice to set pointer address as NULL after call to free.
	//NULL is a macro for address 0 amd it cannot be dereferenced. The code brlow this statement will cause a crash;

	//A[2] = 6; just to test something. This is bad because we dont have memory allocated so anything can happen.

	for(int i = 0; i < n; i++){
		printf("%d ", A[i]);
	}

	printf("\n");

}
