package approxPi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

public class Main{

	public static void main(String[] args) throws IOException, InterruptedException {
		
		
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		String [] inputLine = input.readLine().split(" ");

		int numberPoints = Integer.parseInt(inputLine[0]); 
		int numberThreads = Integer.parseInt(inputLine[1]);
		
		long time = System.nanoTime();
		System.err.println(time);
		Thread[] threads = new Thread[numberThreads];
		Main x = new Main();
		for(int m = 0; m < numberThreads; m++) {
			threads[m] = x.new ApproxPi(numberPoints/numberThreads);
			threads[m].start();
		}
		// Wait until all threads are finish
		int count = 0;
		for (int i = 0; i < numberThreads ; i++) {
			threads[i].join();
		    count += ((ApproxPi)threads[i]).getCount();
		}
		System.out.println("time" + (System.nanoTime()-time));
		System.out.println("\nFinished all threads " + (count*4)/(double)numberPoints);

	}

	public class ApproxPi extends Thread{

		private int numberPoints;
		private int count;

		ApproxPi(int number) {

			numberPoints = number;
			count = 0;
		}

		public void run(){
			try
			{ 
				for(int i = 0; i < numberPoints; i++) {

					ThreadLocalRandom r = ThreadLocalRandom.current();
					double  low = 0;
					double  high = 1;
					double 	x =  low + (high - low) * r.nextDouble();
					double 	y =  low + (high - low) * r.nextDouble();

					if((x*x + y*y) <= 1) {
						count++;
					}
				}
			} catch(Exception e) {

			}
		}
		public int getCount() {
			return count;
		}
	}

}
