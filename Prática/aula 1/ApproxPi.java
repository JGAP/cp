import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * 
 */

/**
 * @author Joao.Pereira
 *
 */
public class ApproxPi {

	public static boolean isInside(double xPos, double yPos) {
		double distance = Math.sqrt((xPos * xPos) + (yPos * yPos));
		return (distance < 1.0);
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("approxPi ");
		String[] inputLine = input.readLine().split(" ");
		int numberOfSimulations = Integer.parseInt(inputLine[0]);

		System.out.print("Total Number of points: ");
		String[] inputLine1 = input.readLine().split(" ");
		Double totalNumberOfPoints = Double.parseDouble(inputLine1[0]);

		// Logic part
		/////////////////////

		Random random = new Random(System.currentTimeMillis());

		double piValue = 0;
		double calcAux = 0;
		int hits = 0;

		long startTime = System.nanoTime();

		for (int i = 0; i < numberOfSimulations; i++) {

			double xPos = (random.nextDouble()) * 2 - 1.0;
			double yPos = (random.nextDouble()) * 2 - 1.0;
			if (isInside(xPos, yPos)) {
				hits++;
			}
		}

		calcAux = (hits / totalNumberOfPoints);
		// System.out.println("calcAux: " + calcAux);
		piValue = calcAux * 4;

		long stopTime = System.nanoTime();

		long elapsedTime = stopTime - startTime;

		System.out.println("Points within circle: " + hits);
		System.out.println("Pi estimation: " + piValue);
		System.out.println("Elapsed Time: " + (elapsedTime / 1000000));
		
		/////////////////////

	}

}
