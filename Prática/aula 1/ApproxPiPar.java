import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * @author Joao.Pereira
 *
 */
public class ApproxPiPar {

	/**
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Total Number of points: ");
		String[] inputLine1 = input.readLine().split(" ");
		int totalNumberOfPoints = Integer.parseInt(inputLine1[0]);

		System.out.print("Number of Threads: ");
		String[] inputLine2 = input.readLine().split(" ");
		int numberOfThreads = Integer.parseInt(inputLine2[0]);

		long startTime = System.nanoTime();
		System.err.println(startTime);

		Thread[] threads = new Thread[numberOfThreads];

		ApproxPiPar startThread = new ApproxPiPar();

		for (int i = 0; i < numberOfThreads; i++) {
			threads[i] = startThread.new MyThread(totalNumberOfPoints / numberOfThreads);
			threads[i].start();
		}

		// Wait until all threads finish
		int count = 0;
		for (int i = 0; i < numberOfThreads; i++) {
			threads[i].join();
			count += ((MyThread) threads[i]).getNumberOfHits();
		}

		long stopTime = System.nanoTime();

		long elapsedTime = stopTime - startTime;

		double piValue = 0;

		piValue = (count * 4) / (double) totalNumberOfPoints;

		System.out.println("Points within circle: " + count);
		System.out.println("Pi estimation: " + piValue);
		System.out.println("Elapsed Time: " + (elapsedTime / 1000000));
	}

	public class MyThread extends Thread {

		private int numberOfPoints;
		private int hits;

		public MyThread(int numberOfPointsPerThread) {
			numberOfPoints = numberOfPointsPerThread;
			hits = 0;
		}

		public boolean isInside(double xPos, double yPos) {
			double distance = Math.sqrt((xPos * xPos) + (yPos * yPos));
			return (distance < 1.0);
		}

		public int getNumberOfHits() {
			return hits;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Random random = new Random(System.currentTimeMillis());

			try {
				for (int i = 0; i < numberOfPoints; i++) {

					double xPos = (random.nextDouble()) * 2 - 1.0;
					double yPos = (random.nextDouble()) * 2 - 1.0;
					if (isInside(xPos, yPos)) {
						hits++;
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

	}
}
